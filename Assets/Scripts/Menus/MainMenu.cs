﻿using UnityEngine;

namespace Menu
{
    public class MainMenu : MonoBehaviour
    {
        // PUBLIC

        public void Quit()
        {
            Application.Quit();
        }
    }
}
